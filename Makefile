# your mileage may vary:
GCC_DIR=C:/ST/STM32CubeIDE/STM32CubeIDE/plugins/com.st.stm32cube.ide.mcu.externaltools.gnu-tools-for-stm32.7-2018-q2-update.win32_1.4.0.202007081208/tools
MAKE_DIR=C:/ST/STM32CubeIDE/STM32CubeIDE/plugins/com.st.stm32cube.ide.mcu.externaltools.make.win32_1.4.0.202007081208/tools



AS=$(GCC_DIR)/bin/arm-none-eabi-as
CC=$(GCC_DIR)/bin/arm-none-eabi-gcc
OBJCOPY=$(GCC_DIR)/bin/arm-none-eabi-objcopy
SIZE=$(GCC_DIR)/bin/arm-none-eabi-size

CFLAGS=-mcpu=cortex-m0 -mthumb -mfloat-abi=soft

PROG=blinky_bare-metal_asm

OBJS=blinky.o

all: $(PROG).bin

$(PROG).bin: $(PROG).elf
	$(OBJCOPY) -Obinary  $< $@

$(PROG).hex: $(PROG).elf
	$(OBJCOPY) -Oihex  $< $@

$(PROG).elf: $(OBJS) flash.ld
	$(CC) $(CFLAGS) -T flash.ld -nostartfiles -nostdlib -o $@ $(OBJS) -Wl,-Map=$(PROG).map
	$(SIZE) $@

clean:
	-$(MAKE_DIR)/bin/rm $(PROG).bin $(PROG).hex $(PROG).elf $(PROG).map $(OBJS)
