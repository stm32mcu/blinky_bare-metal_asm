Adapted for [Nucleo-F042K6](https://os.mbed.com/platforms/ST-Nucleo-F042K6/) board.

Build the exectutable using `make` and the arm-none-eabi toolchain.

For Windows, the make utility comes with STM32CubeIDE:

C:\ST\STM32CubeIDE_1.1.0\STM32CubeIDE\plugins\com.st.stm32cube.ide.mcu.externaltools.make.win32_1.1.0.201910081157\tools\bin\make.exe

or can be downloaded separately.

Your paths may vary, check Makefile.

Flash (program) the .bin file conveniently by connecting the board over USB and dragging the binary to the USB "drive" NODE_F042K6.